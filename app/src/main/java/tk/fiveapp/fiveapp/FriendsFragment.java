package tk.fiveapp.fiveapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.squareup.picasso.Request;

import org.apache.log4j.chainsaw.Main;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import tk.fiveapp.fiveapp.R;

public class FriendsFragment extends ListFragment implements View.OnClickListener {

    View view;
    Button chatButton;

    final String MESSAGES_ENDPOINT = "http://fiveapp.tk:3000";

    ArrayList<String> list_friends;

    public FriendsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        String s1 = intent.getStringExtra("Check");

        if(s1 != null && s1.equals("1"))
        {
            s1 = "";
            Fragment fragment = new FriendsFragment();
            if (fragment != null) {
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment).commit();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_friends, container, false);
        chatButton = (Button) view.findViewById(R.id.chat_button);
        chatButton.setOnClickListener(this);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        SharedPreferences prefs = getActivity().getSharedPreferences("Account", Context.MODE_PRIVATE);
        String token = prefs.getString("token","");
        //Log.d("MyApp","token: " + token);

        //we make a GET call to get the list of friends
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", "bearer " + token);
        client.get(MESSAGES_ENDPOINT + "/listFriends", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                //convert JSONArray to String array
                list_friends = new ArrayList<String>();
                for(int i = 0; i < response.length(); i++){
                    try {
                        list_friends.add(response.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                //we display the list of friends in the listview
                ArrayAdapter adapter = new ArrayAdapter (getActivity(), android.R.layout.simple_list_item_1,
                        list_friends);
                setListAdapter(adapter);

            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //if invalid token, we move to LoginActivity
                if (statusCode == 401){
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
                Log.d("MyApp","Start3 " +responseString+" Status code: "+statusCode);
            }

        });

        //getListView().setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //we start ChatActivity
        Intent intent = new Intent(getActivity().getApplicationContext(), ChatActivity.class);
        intent.putExtra("username", ((MainActivity)getActivity()).username);
        startActivity(intent);
    }

    //convert JSONArray to String array
    public static String[] getStringArray(JSONArray jsonArray){
        String[] stringArray = null;
        int length = jsonArray.length();
        if(jsonArray!=null){
            stringArray = new String[length];
            for(int i=0;i<length;i++){
                stringArray[i]= jsonArray.optString(i);
            }
        }
        return stringArray;
    }
}