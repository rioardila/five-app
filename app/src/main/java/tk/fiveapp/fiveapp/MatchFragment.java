package tk.fiveapp.fiveapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import tk.fiveapp.fiveapp.R;

public class MatchFragment extends Fragment{

    View view;
    TextView textName;

    final String MESSAGES_ENDPOINT = "http://fiveapp.tk:3000";

    public MatchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_match, container, false);

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        SharedPreferences prefs = getActivity().getSharedPreferences("Account", Context.MODE_PRIVATE);
        String username = prefs.getString("username","");
        String token = prefs.getString("token","");

        //we reference the TextViews
        textName = (TextView) view.findViewById(R.id.textName);

        //we make a GET call to get the list of friends
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", "bearer " + token);
        client.get(MESSAGES_ENDPOINT + "/getName/" + username, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject result) {
                textName.setText(result.toString());
                Log.d("MyApp","TODO OK! " + result+" Status code: "+statusCode);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                //if invalid token, we move to LoginActivity
                if (statusCode == 401){
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
                Log.d("MyApp","Name incorrecto " +responseString+" Status code: "+statusCode);

                //we remove the ""
                responseString.substring(1, responseString.length() - 1);
                Log.d("MyApp","Resultado string: " + responseString);
                textName.setText(responseString);
            }

        });
    }

}