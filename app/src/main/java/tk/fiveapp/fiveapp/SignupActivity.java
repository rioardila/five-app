package tk.fiveapp.fiveapp;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.Base64;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;


public class SignupActivity extends ActionBarActivity  {

    public static final String MESSAGES_ENDPOINT = "http://46.101.215.26:3000";
    public static final String UPLOAD_KEY = "image";

    private static final int SELECT_PICTURE = 0;

    private Uri photoPath;
    private Bitmap bitmap;

    EditText usernameInput;
    EditText passwordInput;
    EditText nameInput;
    EditText ageInput;
    Button signupButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        usernameInput = (EditText) findViewById(R.id.username_input);
        passwordInput = (EditText) findViewById(R.id.password_input);
        nameInput = (EditText) findViewById(R.id.name_input);
        ageInput = (EditText) findViewById(R.id.age_input);
        signupButton = (Button) findViewById(R.id.signup_button);

        signupButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String username = usernameInput.getText().toString();
                String password = passwordInput.getText().toString();
                String name = nameInput.getText().toString();
                String ageString = ageInput.getText().toString();

                //check if there is any empty field
                if(username.matches("") || password.matches("") || name.matches("") ||
                        ageString.matches("")) {
                    Toast.makeText(getApplicationContext(), "There are empty fields!", Toast.LENGTH_SHORT).show();
                    return;
                }

                //FIRST OF ALL, WE UPLOAD IMAGE TO SERVER
                uploadImage();

                Integer age = Integer.parseInt(ageString);

                RequestParams params = new RequestParams();

                params.put("username", username);
                params.put("password", password);
                params.put("name", name);
                params.put("age", age);

                AsyncHttpClient client = new AsyncHttpClient();

                client.post(MESSAGES_ENDPOINT + "/signup", params, new JsonHttpResponseHandler(){

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        //we save the username in preferences
                        SharedPreferences prefs = getSharedPreferences("Account",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("username", username);
                        editor.commit();

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("username", username);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Registration successful", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if(statusCode == 401) {
                            Toast.makeText(getApplicationContext(), "User "+username+" already exists!", Toast.LENGTH_LONG).show();
                        }
                        else Toast.makeText(getApplicationContext(), "Something went wrong :(", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

    }


    //CODE FOR PICKING UP A PICTURE FROM LOCAL GALLERY

    public void pickPhoto(View view) {
        //launch the photo picker
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), SELECT_PICTURE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && data != null && data.getData() != null) {
            photoPath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), photoPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //convert bitmap to base64 string
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void uploadImage() {
        class UploadImage extends AsyncTask<Bitmap, Void, String> {

            ProgressDialog loading;
            RequestHandler rh = new RequestHandler();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(SignupActivity.this,
                        "Registering " + usernameInput.getText().toString() + " into FiVe" ,
                        "Please wait...", true, true);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                String uploadImage = getStringImage(bitmap);

                HashMap<String, String> data = new HashMap<>();
                data.put(UPLOAD_KEY, uploadImage);

                String result = rh.sendPostRequest(MESSAGES_ENDPOINT + "/uploadImage", data);

                return result;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
