package tk.fiveapp.fiveapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by rioardila on 24/07/16.
 */
public class LoginActivity extends ActionBarActivity {

    final String MESSAGES_ENDPOINT = "http://46.101.215.26:3000";

    EditText usernameInput;
    EditText passwordInput;
    Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameInput = (EditText) findViewById(R.id.user_input);
        passwordInput = (EditText) findViewById(R.id.pass_input);
        loginButton = (Button) findViewById(R.id.login_button);

        SharedPreferences prefs = getSharedPreferences("Account", Context.MODE_PRIVATE);
        usernameInput.setText(prefs.getString("username",""));

        //set focus to password editText if username already exists
        if(!usernameInput.getText().toString().isEmpty()) passwordInput.requestFocus();

        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //hide keyboard
                try  {
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {

                }

                final String username = usernameInput.getText().toString();
                String password = passwordInput.getText().toString();

                //check if there is any empty field
                if(username.matches("") || password.matches("")) {
                    Toast.makeText(getApplicationContext(), "There are empty fields!", Toast.LENGTH_SHORT).show();
                    return;
                }

                RequestParams params = new RequestParams();

                params.put("username", username);
                params.put("password", password);

                AsyncHttpClient client = new AsyncHttpClient();

                client.post(MESSAGES_ENDPOINT + "/login", params, new JsonHttpResponseHandler(){

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        //we save the username in preferences
                        SharedPreferences prefs = getSharedPreferences("Account",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("username", username);
                        //save token in Preferences
                        try {
                            editor.putString("token",response.getString("token"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        editor.commit();

                        //we start MainActivity
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        //intent.putExtra("username", username);
                        startActivity(intent);
                        finish();
                        //Toast.makeText(getApplicationContext(), "Log in successful: "+response, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if(statusCode == 401) {
                            Toast.makeText(getApplicationContext(), "Username or password invalid!", Toast.LENGTH_LONG).show();
                        }
                        else Toast.makeText(getApplicationContext(), "Something went wrong :(", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
    }

    public void signUp(View view) {
        Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
        startActivity(intent);
    }
}
